/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package causecode;

import java.util.Date;

/**
 *
 * @author My Comp
 */
class Cards 
{
    private String name;
    private String id;
    private String status;
    private Date createddate;
    private Date donedate;
    private String time;

    public Cards(String name, String id, String status, Date createddate, Date donedate,String time) {
        this.name = name;
        this.id = id;
        this.status = status;
        this.createddate = createddate;
        this.time=time;
        this.donedate = donedate;
    }
    
    public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getName() {
        return name;
    }
   
	public Date getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getDonedate() {
		return donedate;
	}

	public void setDonedate(Date donedate) {
		this.donedate = donedate;
	}

	public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    
}
