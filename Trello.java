/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package causecode;

//  Packages

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

/* Created by Akshay Bhatt, BT13CSE008

TO calculate how long it take to change status from "In progress" to "done"
 */
public class Trello 
{
	ArrayList<Cards> cardsList =null;

	public String getAllCardsOnBoard() 
	{
		try
		{
			URL url=new URL("https://api.trello.com/1/boards/CMV0pT47/cards?fields=name,id&key=1096b18be023642d8a1317b0aa964004");
			HttpURLConnection con=(HttpURLConnection)url.openConnection();
			con.connect();
			String str="";
			BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
			String data="";
			while((str=br.readLine())!=null)
			{
				data+=str;
			}

			return data;
		}
		catch(Exception e)
		{
			System.out.println("Error occured in getting all the cards on board");
			e.printStackTrace();
			return null;
		}
	}


		public void calculateTimeElapsed(String cardsOnBoard)
		{
			try
			{
				JSONArray obj=new JSONArray(cardsOnBoard);
				cardsList =new ArrayList<Cards>();
				
				// Details of each card on board
				for (int i=0;i<obj.length();i++)
				{
					JSONObject o=obj.getJSONObject(i);
					String cardId= o.getString("id");
					String cardName= o.getString("name");
					String status=getCardStatus(cardId);
					String cardDetails=getCardDetails(cardId);

					Cards cd= null;
					if (status.equals("Done"))
					{
						Date doneDate=fetchDoneDate(cardDetails);
						Date startDate=fetchStartDate(cardId);
						cd=new Cards(cardName,cardId,status,startDate,doneDate,getDiff(startDate,doneDate));
					}
					else
					{
						cd=new Cards(cardName,cardId,status,fetchStartDate(cardId),null,null);
					}

					// Arraylist to store each card and required informations
					cardsList.add(cd);

				}

			}
			catch(Exception e)
			{
				System.out.println("Error occured in calculating elapsed time");
				e.printStackTrace();
				
			}

		}


		public  String getDiff(Date start,Date done)
		{
			try
			{
				long difference = done.getTime() - start.getTime(); 
                                long millis=difference%1000;
				long seconds = difference / 1000;
				long minutes = seconds / 60;
				long hours = minutes / 60;
				long days = hours / 24;
				String time = days + " days:" + hours % 24 + " hours:" + minutes % 60 + " mins:" + seconds % 60 + " secs:" + millis + " millis"; 

				return time;
			}
			catch(Exception e)
			{
				System.out.println("Error occured in getting difference in start and done time");
				e.printStackTrace();
				return null;
			}

		}
		public  Date fetchStartDate(String id)
		{
			try
			{        
				return new Date(Long.parseLong(id.substring(0,8),16)*1000);
			}
			catch(Exception e)
			{
				System.out.println("Error occured in fetching cards InProgress date");
				e.printStackTrace();
				return null;
			}
		}


		public  Date fetchDoneDate(String data)
		{
			try
			{
				JSONArray obj=new JSONArray(data);
				String doneDate= obj.getJSONObject(0).getString("date");

				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
				format.setTimeZone(TimeZone.getTimeZone("GMT"));
				Date date = format.parse(doneDate);
				return date;  
			}
			catch(Exception e)
			{
				System.out.println("Error occured in fetching cards done date");
				e.printStackTrace();
				return null;
			}
		}

		public String getCardDetails(String cardId)
		{
			try
			{
				URL url=new URL("https://api.trello.com/1/cards/"+cardId+"/actions?fields=date&key=1096b18be023642d8a1317b0aa964004");
				HttpURLConnection con=(HttpURLConnection)url.openConnection();
				con.connect();
				String str;
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String data="";
				while((str=br.readLine())!=null)
				{
					data+=str;
				}
				return data;
			}
			catch(Exception e)
			{
				System.out.println("Error occured in getting card details");
				e.printStackTrace();
				return null;
			}
		}

		public String getCardStatus(String cardId)
		{
			try

			{
				URL url=new URL("https://api.trello.com/1/cards/"+cardId+"/list?key=1096b18be023642d8a1317b0aa964004");
				HttpURLConnection con=(HttpURLConnection)url.openConnection();
				con.connect();
				String str="";
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String data="";
				while((str=br.readLine())!=null)
				{
					data+=str;
				}
				JSONObject obj=new JSONObject(data);
				return obj.getString("name");

			}

			catch(Exception e)
			{
				System.out.println("Error occured in getting card status");
				e.printStackTrace();
				return null;
			}
		}

		public void printDetails(){

			for(Cards cd:cardsList)
			{
				System.out.println("Name: "+cd.getName());
				System.out.println("Card Id:  "+cd.getId()+"\t");
				System.out.println("Status:\t"+cd.getStatus()+"\t");
				System.out.println("start time:\t"+cd.getCreateddate()+"\t");
				if(cd.getDonedate()==null)
				{
					System.out.println("completed time:\t On Progress");
				}
				else{
					System.out.println("completed time:\t"+cd.getDonedate());
				}
				if(cd.getTime()==null)
				{
					System.out.println("Time taken ( progress to done):\t Cannot Calculate");
				}
				else{
					System.out.println("Time taken ( progress to done):\t "+cd.getTime());
				}

				System.out.println("**************************************");
				
			}
			
		}
	}
