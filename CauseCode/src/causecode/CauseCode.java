package causecode;

/* Created by Akshay Bhatt, BT13CSE008
 TO calculate how long it take to change status from "In progress" to "done"
*/
public class CauseCode {

	public static void main(String args[])
	{
            
			Trello trello= new Trello();
			String cardsOnBoard = trello.getAllCardsOnBoard();
			trello.calculateTimeElapsed(cardsOnBoard);
			trello.printDetails();
		
	}
}
